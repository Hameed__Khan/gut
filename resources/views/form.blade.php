<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<style type="text/css">
		.divider-text {
		    position: relative;
		    text-align: center;
		    margin-top: 15px;
		    margin-bottom: 15px;
		}
		.divider-text span {
		    padding: 7px;
		    font-size: 12px;
		    position: relative;   
		    z-index: 2;
		}
		.divider-text:after {
		    content: "";
		    position: absolute;
		    width: 100%;
		    border-bottom: 1px solid #ddd;
		    top: 55%;
		    left: 0;
		    z-index: 1;
		}

		.btn-facebook {
		    background-color: #405D9D;
		    color: #fff;
		}
		.btn-twitter {
		    background-color: #42AEEC;
		    color: #fff;
		}
	</style>
</head>
<body>

	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">




<div class="card bg-light">
<article class="card-body mx-auto" style="max-width: 400px; min-width: 300px;">
	<h4 class="card-title mt-3 text-center">Create Account</h4>
	<hr>
	@if(session()->has('error_msg'))
    <div class="row">
      <div class="col-lg-12 alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="width: auto !important;">×</button>
          <p> {{ session()->get('error_msg') }} </p>
      </div>
    </div>
    @endif

    @if(session()->has('success_msg'))
    <div class="row">
      <div class="col-lg-12 alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="width: auto !important;">×</button>
          <p> {{ session()->get('success_msg') }} </p>
      </div>
    </div>
    @endif
	<br>
	<form method="post" action="{{ url('user/register') }}">
		@csrf
	<div class="form-group input-group">
        <input name="username" class="form-control" placeholder="Username" type="text">
    </div> 

    <div class="form-group input-group">
        <input name="first_name" class="form-control" placeholder="First Name" type="text">
    </div> 

    <div class="form-group input-group">
        <input name="last_name" class="form-control" placeholder="Last Name" type="text">
    </div> 

    <div class="form-group input-group">
        <input name="email" class="form-control" placeholder="Email address" type="email">
    </div>


    <div class="form-group input-group">
        <input class="form-control" name="password" placeholder="password" type="password">
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block"> Create Account  </button>
    </div>                                                                  
</form>
</article>
</div>

</div> 


</body>
</html>