<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $data = $request->all();
        $validatior = Validator::make($data, [
            'username' => ['required', 'string'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'email' => ['required', 'string', 'email'],
            'password' => ['required'],
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->with(['error_msg'=>'Please fill all field']);
        }
        else
        {   
            $jsonString = file_get_contents(base_path('public/data.json'));
            $o_data = json_decode($jsonString, true);
            $id = count($o_data);
            $user_id = [
                "id" => count($o_data)
            ];
            unset($data['_token']);
            array_push($o_data,array_merge($data,$user_id));
            $newJsonString = json_encode($o_data, JSON_PRETTY_PRINT);
            file_put_contents(base_path('public/data.json'), stripslashes($newJsonString));
            return redirect()->back()->with(['success_msg'=>'data saved successfully']);
        }
    }
}
