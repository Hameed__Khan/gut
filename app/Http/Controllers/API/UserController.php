<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;


class UserController extends Controller
{
    public function register(Request $request)
    {
        $data = $request->all();
        $validatior = Validator::make($data, [
            'username' => ['required', 'string'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'email' => ['required', 'string', 'email'],
            'password' => ['required'],
        ]);

        if ($validatior->fails())
        {
            return response()->json(['msg'=>"Please fill all field",'status'=>0]);
        }
        else
        {   
            $jsonString = file_get_contents(base_path('public/data.json'));
            $o_data = json_decode($jsonString, true);
            $id = count($o_data);
            $user_id = [
                "id" => count($o_data)
            ];
            array_push($o_data,array_merge($data,$user_id));
            $newJsonString = json_encode($o_data, JSON_PRETTY_PRINT);
            file_put_contents(base_path('public/data.json'), stripslashes($newJsonString));
            return response()->json(['user_id' => $id,'msg'=>"User data save successfully",'status'=>1]);
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $jsonString = file_get_contents(base_path('public/data.json'));
        $o_data = json_decode($jsonString, true);
        
        if ($data['id']!="") {
             foreach($o_data as $key => $d){
                if ($d['id']==$data['id']) {
                    if (isset($data['username'])) {
                        $o_data[$key]['username'] = $data['username'];
                    }
                    if (isset($data['first_name'])) {
                        $o_data[$key]['first_name'] = $data['first_name'];
                    }
                    if (isset($data['last_name'])) {
                        $o_data[$key]['last_name'] = $data['last_name'];
                    }
                    if (isset($data['email'])) {
                        $o_data[$key]['email'] = $data['email'];
                    }
                    if (isset($data['password'])) {
                        $o_data[$key]['password'] = $data['password'];
                    }
                }
             }

            $newJsonString = json_encode($o_data, JSON_PRETTY_PRINT);
            file_put_contents(base_path('public/data.json'), stripslashes($newJsonString));
            return response()->json(['msg'=>"User data updated successfully",'status'=>1]);
        }
        else{
            return response()->json(['msg'=>"Please fill send user id",'status'=>0]);
        }
        

    }

    public function view()
    {
        $jsonString = file_get_contents(base_path('public/data.json'));

        return response()->json(['data' => $jsonString,'msg'=>"User data save successfully",'status'=>1]);
    }
}
