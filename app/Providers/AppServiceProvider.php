<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Response::macro('attachment', function ($content) {

            $headers = [
                'Content-type' => 'text/json',
                'Content-Disposition' => "attachment; filename='data.json'",
            ];

            return \Response::make($content, 200, $headers);

        });
    }
}
